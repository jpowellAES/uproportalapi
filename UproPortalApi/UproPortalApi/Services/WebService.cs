﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UproPortalApi.UPROWebService;

namespace UproPortalApi.Services
{
    public class WebService
    {
        public static string UproUsername = "WEBSERVICE";
        public static string UproPassword = "UPROWebService";

        public static UPROWebServiceClient get()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            return new UPROWebServiceClient();
        }
    }
}