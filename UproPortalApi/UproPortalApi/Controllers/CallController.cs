﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using UproPortalApi.Models;
using UproPortalApi.Services;
using UproPortalApi.UPROWebService;

namespace UproPortalApi.Controllers
{
    public class CallController : ApiController
    {
        public static string ERROR = "0";
        public static string OK = "1";
        public static string CHANGED_USERNAME = "2";

        [Route("api/{*route}")]
        [AcceptVerbs("GET", "POST", "DELETE", "PUT", "OPTIONS", "HEAD", "PATCH")]
        public HttpResponseMessage Call(string route, [FromBody] JObject requestBody)
        {
            Status status = new Status(false, "You have been logged out due to inactivity");
            User user = Models.User.fromSession(HttpContext.Current);
            if (user.Authenticated)
            {
                UPROWebServiceClient ws = WebService.get();
                try
                {
                    var bodyData = "";
                    if(requestBody != null && requestBody["data"] != null)
                    {
                        if (requestBody["data"] is JObject)
                        {
                            bodyData = ((JObject)requestBody["data"]).ToString(Formatting.None);
                        }
                        else
                        {
                            bodyData = (string)requestBody["data"];
                        }
                    }
                    UPROPortalResponse res = ws.PortalCall(user.UserName, user.Password, HttpContext.Current.Request.HttpMethod, route, bodyData);
                    byte[] data = null;
                    if(res.Body.Length > 0)
                        data = Convert.FromBase64String(res.Body[0].BodyContent);
                    string body = Encoding.UTF8.GetString(data);
                    body = Regex.Replace(body, @"[\u0000-\u001F]", string.Empty);
                    if (res.ResponseCode != OK)
                    {
                        if (res.ResponseCode == CHANGED_USERNAME)
                        {
                            JObject d = (JObject)JsonConvert.DeserializeObject(body);
                            user.UserName = (string)d["changedUsername"];
                        }
                        else
                        {
                            status.Message = res.ResponseMessage;
                            return generateResponse(HttpStatusCode.BadRequest, status);
                        }
                    }
                    return generateResponse(HttpStatusCode.OK, body);
                }catch(Exception e)
                {
                    status.Message = "Could not connect";
                    return generateResponse(HttpStatusCode.BadGateway, status);
                }
            }
            JObject logoutAction = new JObject();
            logoutAction.Add("reason", "expiredSession");
            return generateResponse(HttpStatusCode.Unauthorized, status, logoutAction);
        }

        private HttpResponseMessage generateResponse(HttpStatusCode code, string body)
        {
            HttpResponseMessage response = Request.CreateResponse(code);
            response.Content = new StringContent(body, Encoding.UTF8, "application/json");
            return response;
        }

        private HttpResponseMessage generateResponse(HttpStatusCode code, Status status, JObject extraValues = null)
        {
            HttpResponseMessage response = Request.CreateResponse(code);
            response.StatusCode = code;
            if(extraValues == null)
            {
                extraValues = new JObject();
            }
            extraValues.Add("success", status.Success);
            extraValues.Add("message", status.Message);
            response.Content = new StringContent(extraValues.ToString(Formatting.None), Encoding.UTF8, "application/json");
            return response;
        }

    }
}
