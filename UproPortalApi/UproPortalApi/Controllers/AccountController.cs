﻿using System.Web;
using System.Web.Http;
using UproPortalApi.Models;

namespace UproPortalApi.Controllers
{
    public class AccountController : ApiController
    {

        [Route("account/login")]
        [HttpPost]
        public LoginStatus Login([FromBody]LoginRequest request)
        {
            User user = Models.User.fromSession(HttpContext.Current, false);
            LoginStatus status = new LoginStatus(false, "Fail to connect");

            if (!user.Authenticated)
            {
                string message;
                if(user.authenticate(request, out message))
                {
                    HttpContext.Current.Session["user"] = user;
                }
                status.Success = user.Authenticated;
                status.Message = message;
                status.Name = user.Name;
            }
            return status;
        }

        [Route("account/logout")]
        [HttpPost]
        public Status Logout()
        {
            User user = Models.User.fromSession(HttpContext.Current, false);
            Status status = new Status(false, "You are not logged in");

            if (user.Authenticated)
            {
                HttpContext.Current.Session.Abandon();
                status.Success = true;
                status.Message = "You have been logged out";
            }
            return status;
        }
    }
}
