﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UproPortalApi.Models
{
    public class Status
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public Status(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}