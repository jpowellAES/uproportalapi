﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UproPortalApi.Models
{
    public class LoginStatus : Status
    {
        public int LengthMinutes { get; set; }
        public string Name { get; set; }

        public LoginStatus(bool success, string message) : base(success, message)
        {
            LengthMinutes = WebApiConfig.SessionExpireMinutes;
        }
    }
}