﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UproPortalApi.Services;
using UproPortalApi.UPROWebService;

namespace UproPortalApi.Models
{
    public class User
    {

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public bool Authenticated { get; set; }

        public DateTime LastAction { get; set; }

        public User()
        {
            UserName = "";
            Name = "";
            Authenticated = false;
            LastAction = DateTime.UtcNow;
        }

        public bool authenticate(LoginRequest login, out string message)
        {
            UPROWebServiceClient ws = WebService.get();
            Authenticated = false;
            message = "Invalid username or password";
            try
            {
                string response = ws.Login(WebService.UproUsername, WebService.UproPassword, login.UserName, login.Password);
                JObject res = JObject.Parse(response);
                string success = (string)res["success"];
                if (success != null && success == "1")
                {
                    UserName = login.UserName;
                    Password = login.Password;
                    Name = (string)res["name"];
                    Authenticated = true;
                    message = "Login successful";
                }
            } catch (Exception e)
            {
                message = "Could not connect";
            }
            LastAction = DateTime.UtcNow;
            return Authenticated;
        }

        public bool checkAuth()
        {
            var now = DateTime.UtcNow;
            if (Authenticated && (now - LastAction).TotalMinutes >= WebApiConfig.SessionExpireMinutes)
            {
                Authenticated = false;
            }
            return Authenticated;
        }

        public static User fromSession(HttpContext context, bool abandon = true)
        {
            var session = context.Session;
            if (session != null)
            {
                if(session["user"] != null && session["user"] is User)
                {
                    if (!((User)session["user"]).checkAuth())
                    {
                        if(abandon) context.Session.Abandon();
                    }
                    else
                    {
                        return (User)session["user"];
                    }
                    
                }
            }
            return new User();
        }
    }
}