﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace UproPortalApi
{
    public static class WebApiConfig
    {
        public static string UrlPrefix { get { return "api"; } }
        public static string UrlPrefixRelative { get { return "~/api"; } }

        public static int SessionExpireMinutes { get { return 20; } }


        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

        }
    }
}
